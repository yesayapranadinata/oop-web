<?php 
	include "Database.php";
	
	 class Student {
		
		
		//menambahkan attribute student 
		public $nrp;
		public $nama;
		public $status = "Aktif";
		
		// method untuk menampilkan semua data mahasiswa
		public function getData(){
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "Select * from mahasiswa";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		
		// method untuk menampilkan data mahasiswa berdasarkan NRP
			public function getDetail($nrp){
				$db = new Database();
				$dbConnect = $db->connect();
				$sql = "select * from mahasiswa where nrp = '{$nrp}'";
				$data = $dbConnect->query($sql);
				$dbConnect = $db->close();
				return $data->fetch_array();
			}


		
		//method create untuk menyimpan daata ke database
		public function create(){
			$db = new Database();
			//membuka koneksi
			$dbConnect = $db->connect();
			
			//query menyimpan data
			$sql = "insert into mahasiswa (nrp,nama,status) values ('{$this->nrp}','{$this->nama}','{$this->status}') ";
			//esekusi query di atas
			$data = $dbConnect->query($sql);
           
			//menampung error query simpan data
			$error = $dbConnect->error;
			//menutup koneksi
			$dbConnect = $db->close();
			//menampilkan nilai error
			return $error;
		}
		
		
		public function update(){
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "UPDATE mahasiswa SET nama = '{$this->nama}', status = '{$this->status}' where nrp = '{$this->nrp}'";
			$data = $dbConnect->query($sql);
			$error = $dbConnect->error;
			$dbConnect = $db->close();
			return $error;
			
		}
		public function delete(){
            $db = new Database();
            $dbConnect = $db->connect();
			$sql = "delete from mahasiswa where nrp = '{$this->nrp}'";
			$data = $dbConnect->query($sql);
			$error = $dbConnect->error;
			$dbConnect = $db->close();
			return $error;
            
        }
	


		}
	
	
	

?>