<?php 
	include "class/Student.php";
	$student = new Student();
	$data = null;
	if(isset($_GET['nrp'])){
		$data = $student->getDetail($_GET['nrp']);
	}
?>

<p>
	<h5> Data Mahasiswa : <?= $_GET['nrp']; ?> </h5>
	<a href="index.php?page=student-create" class="btn btn-success"> Tambah </a>
	<a href="index.php?page=student" class="btn btn-success"> Kembali </a>
</p>


<P>
	<?php if($data) : ?>
		<table class="table table-striped table-hover table-dark" >
			<tr>
				<th class="text-right"> NRP </th>
				<td> <?= $data['nrp'] ?> </td>
			</tr>
			<tr>
				<th class="text-right"> Nama </th>
				<td> <?= $data['nama'] ?> </td>
			</tr>
			<tr>
				<th class="text-right"> Status </th>
				<td> <?= $data['status'] ?> </td>
			</tr>
		</table>
	<?php endif; ?>
</p>
