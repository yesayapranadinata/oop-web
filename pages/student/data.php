<?php
	include "class/Student.php";
	$student = new Student();
?>

<p>
	<h5> Data Mahasiswa </h5>
	<a href="index.php?page=student-create" class="btn btn-success"> Tambah </a>
    
</p>

<p>
	<table class="table table-striped table-hover table-dark">
		<thead>
			<tr>
				<th class="text-center"> NRP </th>
				<th class="text-center"> Nama </th>
				<th class="text-center"> Keterangan </th>
				<th class="text-center"> Aksi </th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($student->getData() as $data) : ?>
				<tr>
					<td align="center"> <?= $data['nrp'] ?> </td>
					<td align="center"> <?= $data['nama'] ?> </td>
					<td align="center"> <?= $data['status'] ?> </td>
					<td class="text-center">
						<a href="index.php?page=student-detail&nrp=<?php echo $data['nrp']; ?>" class="btn btn-primary"> Info </a>	
						<a href="index.php?page=student-update&nrp=<?php echo $data['nrp']; ?>" class="btn btn-success"> Ubah </a>
						<a href="index.php?page=student-delete&nrp=<?php echo $data['nrp']; ?>" class="btn btn-warning"> Hapus </a>         
					</td>
				</tr>
				<?php endforeach ?> 
		</tbody>
	</table>
</p>