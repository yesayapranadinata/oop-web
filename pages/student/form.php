		<?php 
	//memulai session
	session_start();
?>

<!-- membuat tampilan form create -->
<?php if($page=="student-create") : ?>
	<!-- Mengecek session message -->
		<?php if(isset($_SESSION['message'])) :  ?>
		<!-- jika terdapat error maka munculkan pesan pada session yang telah di buat-->
		<p>
			<div class="alert alert-danger" role="alert">
			Gagal Menyimpan Data : <?= $_SESSION['message'] ?>
			</div>		
		</p>
		<!-- Mengosongkan session message agar pesan tidak muncul kembali -->
			<?php unset($_SESSION['message']); ?>
		<?php endif; ?>

<!-- Form tambah mahasiswa status diisi default dari class -->
<p>

	<h5> Tambah Data Mahasiswa </h5>
	<form method="post" action="controllers/student/create.php">
		<div class="form-group"  >
			<label for="nrp">NRP</label>
			<input type="text" class="form-control" name="nrp" placeholder="NRP" required >
		</div>
		<div class="form-group"  >
			<label for="nrp">Nama</label>
			<input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" required >
		</div>
			<button type="Submit" class="btn btn-success">Simpan</button>
	</form>	
</p>
<?php endif; ?>

<!-- membuat tampilan form update -->

<?php if($page=="student-update") : ?>
	<?php 
		//sama seperti tampilan detail, untuk membuat form update 
		// diperlukan 1 data dari student 
		include "class/Student.php";
		$student = new Student();
		$data = null;
		if(isset($_GET['nrp'])){
			//mengambil semua data berdasarkan nrp 
			$data = $student->getDetail($_GET['nrp']);
		}
	?>
	<p> 
		<h5> Ubah Data Mahasiswa : <?= $_GET['nrp']; ?> </h5>
		<form method="post" action="controllers/student/update.php">
			<div class="form-group">
<!--
			 NRP di sembunyikan, karena primary key tidak boleh di ubah 
				ganti semua placeholder menjadi value 	
--> 
			
			<input type="hidden" class="form-control" name="nrp" value="<?= $data['nrp'] ?>" >
			</div>
			<!-- <select name="nama" id="select" class="form-control">
			<?php 
                //include "../../class/Student.php";
                //$student = new Student();
            ?>
                <?php //foreach ($student->getData() as $data): ?>
                  <option value="<?= $data['nama']; ?>"><?= $data['nama']; ?></option>        
                <?php //endforeach ?> 
             </select> -->
             
            <div class="form-group">
				<label for="nama"> Nama </label>
				<input type="text" class="form-control" name="nama" value="<?= $data['nama'] ?>">
			</div>

			<div class="form-group">
				<label for="status"> Status </label>
				<input type="text" class="form-control" name="status" value="Sangat Aktif" readonly>
			</div>
			
			<button type="submit" class="btn btn-success mb-2"> Ubah </button>

		</form>
	</p>
<?php endif; ?>



<?php if($page=="student-delete") : ?>
<!-- Form delete mahasiswa status diisi default dari class -->
<!-- Mengecek session message -->
<?php 
		//sama seperti tampilan detail, untuk membuat form update 
		// diperlukan 1 data dari student 
		include "class/Student.php";
		$student = new Student();
		$data = null;
		if(isset($_GET['nrp'])){
			//mengambil semua data berdasarkan nrp 
			$data = $student->getDetail($_GET['nrp']);
		}
?>

<p>

	<h5> Hapus Data Mahasiswa : <?= $_GET['nrp']; ?>  </h5>
	<form method="post" action="controllers/student/delete.php">
		<div class="form-group"  >
            <!-- mengambil imputan yang di hidden sehingga imputan sudah terisi namun tidak bisa di ganti sesuai dengan dimana tombol delete 
                 tersebut -->
            <input type="hidden" class="form-control" name="nrp" value="<?= $data['nrp'] ?>" >
		</div>    
			<button type="Submit" class="btn btn-success">Simpan</button>
            <a href="index.php?page=student" class="btn btn-success"> Kembali </a>
			
        
	</form>	
</p>
<?php endif; ?>

	
	
	
	
	
	
	
	
	
	
	
	
	
	