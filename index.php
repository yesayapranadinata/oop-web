<html>
	<head>
		<title> Portal Mahasiswa </title>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
		<script src="bootstrap/jquery.min.js"> </script>
		<script src="bootstrap/js/bootstrap.js"> </script>
	</head>
	
	<body>
			<?php 
				//cek halaman yang di tuju
				array_key_exists('page', $_GET) ? $page = $_GET['page'] : $page = '';
			?>
		
		<!-- Menu -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#"> Portal Mahasiswa </a>
			
			<div class="collapse navbar-collapse" id="navbarSupportedContent" >
				<ul class="navbar-nav mr-auto" >
					<li class="nav-item <?php if(!$page || $page=='home') echo 'active'; ?>" >
						<a class="nav-link" href="index.php?page=home" > Home </a>
					</li>
					<li class="nav-item <?php if($page=='student') echo 'active'; ?>" >
						<a class="nav-link" href="index.php?page=student" > Mahasiswa </a>
					</li>
                    <li class="nav-item <?php if($page=='dosen') echo 'active'; ?>" >
						<a class="nav-link" href="index.php?page=dosen" > Dosen </a>
					</li>
				</ul>
			</div>
		</nav>
		<!-- End Menu -->
		
		<div class="container" >			
			<?php 
				//redirect ke halaman data mahasiswa
				if($page=="student"){
					include "pages/student/data.php";
				}
				
				//redirect ke halaman detail mahasiswa
				if($page=="student-detail"){
					include "pages/student/detail.php";
				}
				
				//redirect ke halaman tambah mahasiswa
				if($page=="student-create"){
					include "pages/student/form.php";
				}
				
				// redirect ke halaman ubah mahasiswa
				if($page=="student-update"){
					include "pages/student/form.php";	
				}
				if ($page=="student-delete"){
                    include "pages/student/form.php";
                }
			?>	
		</div>
		<!-- End Content -->
		
		
	</body>

</html>