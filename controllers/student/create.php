<?php 
	include "../../class/Student.php";
	$student = new Student();
	
	//mengisi attribute dengan hasil imputan 
	$student->nrp = $_POST['nrp'];
	$student->nama = $_POST['nama'];
	
	//menampung hasil dari method crate 
	$error = $student->create();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		//memanggil tampilan detail dengan mengirimkan page dan nrp
		header("location: ../../index.php?page=student-detail&nrp={$student->nrp}"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=student-create");
	}

	
?>
