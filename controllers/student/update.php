<?php 
include "../../class/Student.php";
	$student = new Student();
	
	//mengisi attribute dengan hasil imputan 
	$student->nrp = $_POST['nrp'];
	$student->nama = $_POST['nama'];
	$student->status = $_POST['status'];
	
	//menampung hasil dari method update
	$error = $student->update();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error){
		//memanggil tampilan detail dengan mengirimkan page dan nrp
		header("location: ../../index.php?page=student-detail&nrp={$student->nrp}"); 
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan update kembali
		header("location: ../../index.php?page=student-update");
	}

?>