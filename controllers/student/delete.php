<?php 
include "../../class/Student.php";
	$student = new Student();
	
	//mengisi attribute dengan hasil imputan 
	$student->nrp = $_POST['nrp']; 
	
	
	//menampung hasil dari method update
	$error = $student->delete();
	

    if(!$error){
		//memanggil tampilan detail dengan mengirimkan page dan nrp
		header("location: ../../index.php?page=student");
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=student-create");
	}

?>